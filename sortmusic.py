import sys


def order_items(songs: list, i: int, gap: int) -> list:

    while i >= gap and songs[i - gap][1] < songs[i][1]:
        songs[i], songs[i - gap] = songs[i - gap], songs[i]
        i -= gap
    return songs


def sort_music(songs: list) -> list:

    n = len(songs)
    gap = n // 2

    while gap > 0:
        for i in range(gap, n):
            songs = order_items(songs, i, gap)

        gap //= 2

    return songs


def create_dictionary(arguments):

    dictionary = {}
    for i in range(0, len(arguments), 2):
        song_name = arguments[i]
        plays = int(arguments[i + 1])
        dictionary[song_name] = plays

    return dictionary


def main():
    args = sys.argv[1:]

    if len(args) % 2 != 0 or len(args) == 0:
        sys.exit("Error: Debes proporcionar pares de canción y número de reproducciones.")

    songs = create_dictionary(args)

    sorted_songs = sort_music(list(songs.items()))

    sorted_dict = dict(sorted_songs)
    print(sorted_dict)


if __name__ == '__main__':
    main()

    main()
